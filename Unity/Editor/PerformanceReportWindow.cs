﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
#if PERFORMANCE && UNITY_5
namespace Codefarts.PerformanceTesting
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Reflection;

    using UnityEditor;
    using UnityEngine;
    using System.Linq;


    /// <summary>
    /// Provides a performance reporting window.
    /// </summary>
    public class PerformanceReportWindow : EditorWindow
    {
        /// <summary>
        /// Holds the scroll value for the performance metrics.
        /// </summary>
        private Vector2 metricsScroll;

        /// <summary>
        /// Holds the performance data report information.
        /// </summary>
        private string performanceData = string.Empty;

        /// <summary>
        /// Holds the selected index for the selected performance metric.
        /// </summary>
        private int selectedIndex;

        /// <summary>
        /// Used to display names for each performance metric.
        /// </summary>
        private readonly string[] names = new[] { "Hierarchy", "Name", "Total", "Average", "Count" };

        /// <summary>
        /// Used to determine how the performance metrics are organized.
        /// </summary>
        private int sortIndex;

        private TreeModel selectedNode;
        private List<TreeModel> treeModels = new List<TreeModel>();

        private Texture2D minusTexture;
        private Texture2D plusTexture;

        private GUIStyle leftAlignButton;

        private bool record;


        public void CreateTextures()
        {
            if (this.minusTexture == null)
            {
                this.minusTexture = new Texture2D(8, 8, TextureFormat.ARGB32, false);
                var colors = new[]
                        {
                            Color.black, Color.black, Color.black, Color.black, Color.black, Color.black, Color.black, Color.black,
                            Color.black, Color.white, Color.white, Color.white, Color.white, Color.white, Color.white, Color.black,
                            Color.black, Color.white, Color.white, Color.white, Color.white, Color.white, Color.white, Color.black,
                            Color.black, Color.white, Color.black, Color.black, Color.black, Color.black, Color.white, Color.black,
                            Color.black, Color.white, Color.black, Color.black, Color.black, Color.black, Color.white, Color.black,
                            Color.black, Color.white, Color.white, Color.white, Color.white, Color.white, Color.white, Color.black,
                            Color.black, Color.white, Color.white, Color.white, Color.white, Color.white, Color.white, Color.black,
                            Color.black, Color.black, Color.black, Color.black, Color.black, Color.black, Color.black, Color.black,
                        };
                this.minusTexture.SetPixels(colors);
                this.minusTexture.Apply();
            }

            if (this.plusTexture == null)
            {
                this.plusTexture = new Texture2D(8, 8, TextureFormat.ARGB32, false);
                var colors = new[]
                        {
                            Color.black, Color.black, Color.black, Color.black, Color.black, Color.black, Color.black, Color.black,
                            Color.black, Color.white, Color.white, Color.white, Color.white, Color.white, Color.white, Color.black,
                            Color.black, Color.white, Color.black, Color.black, Color.black, Color.black, Color.white, Color.black,
                            Color.black, Color.white, Color.black, Color.black, Color.black, Color.black, Color.white, Color.black,
                            Color.black, Color.white, Color.black, Color.black, Color.black, Color.black, Color.white, Color.black,
                            Color.black, Color.white, Color.black, Color.black, Color.black, Color.black, Color.white, Color.black,
                            Color.black, Color.white, Color.white, Color.white, Color.white, Color.white, Color.white, Color.black,
                            Color.black, Color.black, Color.black, Color.black, Color.black, Color.black, Color.black, Color.black,
                        };
                this.plusTexture.SetPixels(colors);
                this.plusTexture.Apply();
            }
        }

        /// <summary>
        /// Called by Unity.
        /// </summary>
        public void OnGUI()
        {
            if (this.leftAlignButton == null)
            {
                this.leftAlignButton = new GUIStyle(GUI.skin.button);
                this.leftAlignButton.alignment = TextAnchor.MiddleLeft;
            }

            this.UpdatePerformanceData(this.selectedNode);

            this.CreateTextures();
            GUILayout.BeginVertical();


            GUILayout.BeginHorizontal();
            // GUILayout.Space(4);
            this.DrawPerformanceMetrics();
            //  GUILayout.Space(4);
            this.DrawPerformanceResults();
            GUILayout.EndHorizontal();

            GUILayout.EndVertical();

            if (this.record)
            {
                this.Repaint();
            }
        }

        /// <summary>
        /// Draws the tool bar header.
        /// </summary>
        private void DrawHeader()
        {
            // GUILayout.Label("Performance");
            //  GUILayout.FlexibleSpace();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Reset All"))
            {
                var perf = PerformanceTesting<string>.Instance;
                perf.ResetAll(true);
                this.treeModels.Clear();
            }

            this.sortIndex = EditorGUILayout.Popup(this.sortIndex, this.names);
            // GUILayout.FlexibleSpace();

            GUI.enabled = !this.record;
            if (GUILayout.Button("Update"))
            {
                this.Repaint();
            }

            GUI.enabled = true;

            this.record = GUILayout.Toggle(this.record, "Live");
            GUILayout.EndHorizontal();
        }

        /// <summary>
        /// Draw the <see cref="performanceData"/> results.
        /// </summary>
        private void DrawPerformanceResults()
        {
            if (!string.IsNullOrEmpty(this.performanceData))
            {
                GUILayout.TextArea(this.performanceData);
            }
        }

        /// <summary>
        /// Draws the performance metrics table.
        /// </summary>
        private void DrawPerformanceMetrics()
        {
            GUILayout.BeginVertical(GUI.skin.box, GUILayout.MaxWidth(300));
            // draw header
            this.DrawHeader();

            foreach (var type in PerformanceTestingTypes.GetTypes())
            {
                this.UpdateTreeModel(type);
            }

            this.metricsScroll = GUILayout.BeginScrollView(this.metricsScroll, false, false, GUI.skin.horizontalScrollbar, GUI.skin.verticalScrollbar, GUI.skin.scrollView);
            GUILayout.BeginVertical();

            if (this.sortIndex == 0)
            {
                // draw tree
                foreach (var model in this.treeModels)
                {
                    this.DrawNode(model, 0);
                }
            }
            else
            {
                this.DrawPerformanceMetricsFlatList();
            }

            GUILayout.EndVertical();

            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }

        private void DrawNode(TreeModel model, int depth)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(depth * 8);

            if (model.Children.Count > 0)
            {
                if (GUILayout.Button(model.Expanded ? this.minusTexture : this.plusTexture, GUI.skin.label))
                {
                    model.Expanded = !model.Expanded;
                }

            }
            else
            {
                GUILayout.Space(depth * 8);
            }

            var style = new GUIStyle(GUI.skin.label);
            style.normal.textColor = this.selectedNode == model ? Color.blue : style.normal.textColor;
            if (GUILayout.Button(model.Name, style))
            {
                model.Expanded = true;
                if (string.IsNullOrEmpty(model.KeyName))
                {
                    this.performanceData = string.Empty;
                }
                else
                {
                    this.selectedNode = model;

                    this.UpdatePerformanceData(model);
                }
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            if (model.Expanded)
            {
                foreach (var item in model.Children)
                {
                    this.DrawNode(item, depth + 1);
                }
            }
        }

        private void UpdatePerformanceData(TreeModel model)
        {
            if (model == null)
            {
                this.performanceData = string.Empty;
                return;
            }

            var performanceTestingType = typeof(PerformanceTesting<>);
            Type[] typeArgs = { model.Type };
            var genericType = performanceTestingType.MakeGenericType(typeArgs);

            var data = genericType.GetProperty("Instance", BindingFlags.GetProperty | BindingFlags.Static | BindingFlags.Public).GetGetMethod().Invoke(null, null);
            var totalMilli = data.GetType().GetMethod("TotalMilliseconds", new[] { model.Type });
            var avgMilli = data.GetType().GetMethod("AverageMilliseconds", new[] { model.Type });
            var totalTicks = data.GetType().GetMethod("TotalTicks", new[] { model.Type });
            var avgTicks = data.GetType().GetMethod("AverageTicks", new[] { model.Type });
            var startCount = data.GetType().GetMethod("GetStartCount", new[] { model.Type });
            var fromConverter = data.GetType().GetProperty("ConvertFromStringCallback");
            // var toConverter = data.GetType().GetProperty("ConvertToStringCallback", new[] { type });
            var func = fromConverter.GetGetMethod().Invoke(data, null);
            var invoker = func.GetType().GetMethod("Invoke");

            var key = invoker.Invoke(func, new[] { model.KeyName });
            this.performanceData = string.Format(
                "{0}\r\nTotal: {1}ms ({4} ticks)\r\nAverage: {2}ms ({5} ticks)\r\nCount: {3}\r\n",
                model.KeyName,
                totalMilli.Invoke(data, new[] { key }),
                avgMilli.Invoke(data, new[] { key }),
                startCount.Invoke(data, new[] { key }),
                totalTicks.Invoke(data, new[] { key }),
                avgTicks.Invoke(data, new[] { key }));
        }

        private class TreeModel
        {
            public string Name;

            public bool Expanded;
            // public bool Selected;

            public Type Type;

            public string KeyName;

            public List<TreeModel> Children = new List<TreeModel>();
        }

        private void UpdateTreeModel(Type type)
        {
            var keyNames = PerformanceTestingTypes.GetKeyNames(type);
            foreach (var keyName in keyNames)
            {
                var items = this.treeModels;
                var parts = keyName.Split(new[] { Path.AltDirectorySeparatorChar.ToString(CultureInfo.InvariantCulture) }, StringSplitOptions.RemoveEmptyEntries);
                for (var partIndex = 0; partIndex < parts.Length; partIndex++)
                {
                    var node = items.FirstOrDefault(x => string.CompareOrdinal(x.Name, parts[partIndex]) == 0);
                    if (node != null)
                    {
                        items = node.Children;
                    }
                    else
                    {
                        var model = new TreeModel() { Name = parts[partIndex], Type = type };
                        if (partIndex == parts.Length - 1)
                        {
                            model.KeyName = keyName;
                        }

                        items.Add(model);
                        items = model.Children;
                    }
                }
            }
        }


        /// <summary>
        /// Draws the performance metrics table.
        /// </summary>
        private void DrawPerformanceMetricsFlatList()
        {
            foreach (var type in PerformanceTestingTypes.GetTypes())
            {
                var performanceTestingType = typeof(PerformanceTesting<>);
                Type[] typeArgs = { type };
                var genericType = performanceTestingType.MakeGenericType(typeArgs);

                var data = genericType.GetProperty("Instance", BindingFlags.GetProperty | BindingFlags.Static | BindingFlags.Public).GetGetMethod().Invoke(null, null);
                var totalMilli = data.GetType().GetMethod("TotalMilliseconds", new[] { type });
                var avgMilli = data.GetType().GetMethod("AverageMilliseconds", new[] { type });
                var totalTicks = data.GetType().GetMethod("TotalTicks", new[] { type });
                var avgTicks = data.GetType().GetMethod("AverageTicks", new[] { type });
                var startCount = data.GetType().GetMethod("GetStartCount", new[] { type });
                var fromConverter = data.GetType().GetProperty("ConvertFromStringCallback");
                // var toConverter = data.GetType().GetProperty("ConvertToStringCallback", new[] { type });
                var func = fromConverter.GetGetMethod().Invoke(data, null);
                var invoker = func.GetType().GetMethod("Invoke");

                var keyNames = PerformanceTestingTypes.GetKeyNames(type);

                switch (this.sortIndex)
                {
                    case 1:
                        keyNames = keyNames.OrderBy(x => x).ToArray();
                        break;

                    case 2:
                        keyNames = keyNames.OrderByDescending(x => totalTicks.Invoke(data, new[] { invoker.Invoke(func, new[] { x }) })).ToArray();
                        break;

                    case 3:
                        keyNames = keyNames.OrderByDescending(x => avgTicks.Invoke(data, new[] { invoker.Invoke(func, new[] { x }) })).ToArray();
                        break;

                    case 4:
                        keyNames = keyNames.OrderByDescending(x => startCount.Invoke(data, new[] { invoker.Invoke(func, new[] { x }) })).ToArray();
                        break;
                }

                this.DrawGenericGrid((items, index, style, options) =>
                {
                    var value = items[index];
                    var selected = GUILayout.Toggle(this.selectedIndex == index, value, style);
                    if (selected)
                    {
                        this.selectedIndex = index;
                        var key = invoker.Invoke(func, new[] { value });
                        this.performanceData = string.Format("{0}\r\nTotal: {1}ms ({4} ticks)\r\nAverage: {2}ms ({5} ticks)\r\nCount: {3}\r\n", value,
                            totalMilli.Invoke(data, new[] { key }),
                            avgMilli.Invoke(data, new[] { key }),
                            startCount.Invoke(data, new[] { key }),
                            totalTicks.Invoke(data, new[] { key }),
                            avgTicks.Invoke(data, new[] { key }));
                    }
                    return value;
                }, keyNames, 1, this.leftAlignButton);
            }
        }

        /// <summary>
        /// Draw a grid of controls using a draw callback similar to SelectionGrid.
        /// </summary>
        /// <param name="drawCallback">Specifies a draw callback that is responsible for performing the actual drawing.</param>
        /// <param name="values">Specifies the values of the controls.</param>
        /// <param name="columns">The number of columns in the grid.</param>
        /// <param name="style">The style to be applied to each control.</param>
        /// <param name="options">Specifies layout options to be applied to each control.</param>
        /// <returns>Returns the value for each control.</returns>
        /// <remarks><p>Controls are drawn top to bottom, left to right.</p>  </remarks>
        /// <exception cref="IndexOutOfRangeException">Can occur if the size of the array is too small.</exception>
        /// <exception cref="ArgumentNullException">If the drawCallback is null.</exception>
        private T[] DrawGenericGrid<T>(Func<T[], int, GUIStyle, GUILayoutOption[], T> drawCallback, T[] values, int columns, GUIStyle style, params GUILayoutOption[] options)
        {
            if (drawCallback == null)
            {
                throw new ArgumentNullException("drawCallback");
            }

            GUILayout.BeginVertical();
            var rowIndex = 0;
            var columnIndex = 0;
            var index = (rowIndex * columns) + columnIndex;

            var results = new T[values == null ? 0 : values.Length];
            GUILayout.BeginHorizontal();
            while (values != null && index < values.Length)
            {
                // draw control
                results[index] = drawCallback(values, index, style, options);

                // move to next column
                columnIndex++;

                // if passed max columns move down to next row and set to first column
                if (columnIndex > columns - 1)
                {
                    columnIndex = 0;
                    rowIndex++;

                    // remember to start a new horizontal layout
                    GUILayout.EndHorizontal();
                    GUILayout.BeginHorizontal();
                }

                // re-calculate the index
                index = (rowIndex * columns) + columnIndex;
            }

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            return results;
        }

        /// <summary>
        /// Shows the <see cref="PerformanceReportWindow"/>.
        /// </summary>
        [MenuItem("Window/Codefarts/Performance/Results")]
        public static void ShowWindow()
        {
            var window = GetWindow<PerformanceReportWindow>("Performance Results");
            window.Show();
        }
    }
}
#endif